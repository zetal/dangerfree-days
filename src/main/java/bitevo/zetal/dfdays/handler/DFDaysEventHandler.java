package bitevo.zetal.dfdays.handler;

import java.util.Random;

import bitevo.zetal.dfdays.config.DFDaysConfig;
import net.minecraft.entity.EntityLiving;
import net.minecraft.entity.EntityLivingBase;
import net.minecraft.entity.monster.IMob;
import net.minecraft.world.World;
import net.minecraftforge.event.entity.EntityJoinWorldEvent;
import net.minecraftforge.event.entity.living.LivingEvent.LivingUpdateEvent;
import net.minecraftforge.event.entity.living.LivingSpawnEvent;
import net.minecraftforge.fml.common.eventhandler.Event.Result;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

public class DFDaysEventHandler {
    private Random rand;

    @SubscribeEvent
    public void onClassicLivingSpawn(LivingSpawnEvent.CheckSpawn event) {
        World world = event.getWorld();
        if (shouldFilter(world, event.getEntityLiving())) {
            if (event.isSpawner()) {
                System.out.println(">>>> It's a spawner even though it shouldn't be???");
            }
            event.setResult(Result.DENY);
        }
    }

    @SubscribeEvent
    public void onSpecialSpawn(LivingSpawnEvent.SpecialSpawn event) {
        World world = event.getWorld();
        if (shouldFilter(world, event.getEntityLiving())) {
            if (!DFDaysConfig.ignoreMobSpawners) {
                event.setCanceled(true);
            }
        }
    }

    @SubscribeEvent
    public void onLivingUpdate(LivingUpdateEvent event) {
        World world = event.getEntityLiving().getEntityWorld();
        long tod = getTimeOfDay(world);
        if (DFDaysConfig.constantRemoval) {
            if (shouldFilter(world, event.getEntityLiving())) {
                if (event.getEntityLiving() instanceof EntityLiving) {
                    EntityLiving el = (EntityLiving) event.getEntityLiving();
                    el.spawnExplosionParticle();
                }
                event.getEntityLiving().setDead();
            }
            // We give the start of the day some small amount of fuzziness to
            // accommodate stubborn, slow-ticking entities
        } else if (tod < DFDaysConfig.dayStart + 200) {
            if (shouldFilter(world, event.getEntityLiving())) {
                if (event.getEntityLiving() instanceof EntityLiving) {
                    EntityLiving el = (EntityLiving) event.getEntityLiving();
                    el.spawnExplosionParticle();
                }
                event.getEntityLiving().setDead();
            }
        }
    }

    public static boolean shouldFilter(World world, EntityLivingBase entity) {
        // Should the mod even apply to this dimension?
        if (isDimensionWhitelisted(world.provider.getDimension()) && entity instanceof IMob) {
            // Is the height of the entity valid for spawning? (includes "we
            // don't care about the height")
            long tod = getTimeOfDay(world);
            //System.out.println(tod + " " + DFDaysConfig.dayStart + " " + DFDaysConfig.dayEnd);
            if (tod < DFDaysConfig.dayStart || tod > DFDaysConfig.dayEnd) {
                // Entity should spawn
                return false;
            } else if (DFDaysConfig.heightException && (entity.getPosition().getY() >= DFDaysConfig.minHeight
                    && entity.getPosition().getY() <= DFDaysConfig.maxHeight)) {
                // Entity should spawn
                return false;
            }
            // Entity should NOT spawn
            return true;
        }
        // Entity should spawn
        return false;
    }
    
    public static long getTimeOfDay(World world) {
        return world.getWorldTime() % 24000L;
    }

    public static boolean isDimensionWhitelisted(int dimensionId) {
        int[] whitelist = DFDaysConfig.getDimensionWhitelist();
        for (int i = 0; i < whitelist.length; i++) {
            if (whitelist[i] == dimensionId) {
                return true;
            }
        }
        return false;
    }
}
