package bitevo.zetal.dfdays.config;

import org.apache.commons.lang3.StringUtils;

import bitevo.zetal.dfdays.DFDaysMod;
import net.minecraftforge.common.config.Config;
import net.minecraftforge.common.config.ConfigManager;
import net.minecraftforge.fml.client.event.ConfigChangedEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;

@Config(modid = DFDaysMod.MODID)
@Config.LangKey("dfdays.config.title")
public class DFDaysConfig
{
	@Config.Name("Safe Day Start")
	@Config.Comment("Time of Minecraft day that Monsters are removed and spawning is disabled.")
	@Config.RangeInt(min = 0, max = 8000)
	public static int dayStart = 0;

	@Config.Name("Safe Day End")
	@Config.Comment("Time of Minecraft day that Monsters spawning is re-enabled.")
	@Config.RangeInt(min = 8000, max = 16000)
	public static int dayEnd = 12000;

	@Config.Name("Ignore Mob Spawners")
	@Config.Comment("Whether mobs spawned by a mob spawner should ignore this mod.")
	public static boolean ignoreMobSpawners = false;

	@Config.Name("Constant Removal")
	@Config.Comment("Whether mobs should be constantly removed if they meet criteria or only when daytime starts." + "\n" + "This impacts performance slightly, but not much.")
	public static boolean constantRemoval = false;

	@Config.Name("Height Override")
	@Config.Comment("Whether mobs within a certain height range should ignore time of day when spawning.")
	public static boolean heightException = true;

	@Config.Name("Override - Min Height")
	@Config.Comment("Minimum height for the height override setting.")
	public static int minHeight = 0;

	@Config.Name("Override - Max Height")
	@Config.Comment("Maximum height for the height override setting.")
	public static int maxHeight = 24;

	@Config.Name("Dimension Whitelist")
	@Config.Comment("Comma-separated list of dimension ID's that this mod should effect." + "\n" + "Example:1,-1" + "\n" + "Warning: Deviations from the required format can cause errors.")
	public static String dimensions = "0";

	@Mod.EventBusSubscriber
	private static class EventHandler
	{
		/**
		 * Inject the new values and save to the config file when the config has been changed from the GUI.
		 */
		@SubscribeEvent
		public static void onConfigChanged(ConfigChangedEvent.OnConfigChangedEvent event)
		{
			if (event.getModID().equals(DFDaysMod.MODID))
			{
				ConfigManager.sync(DFDaysMod.MODID, Config.Type.INSTANCE);
			}
		}
	}

	public static int[] getDimensionWhitelist()
	{
		String dims = dimensions.replace(" ", "");
		String[] d = StringUtils.split(dims, ",");
		int[] results = new int[d.length];
		for (int i = 0; i < d.length; i++)
		{
			try
			{
				results[i] = Integer.parseInt(d[i]);
			}
			catch (NumberFormatException nfe)
			{
				System.err.println(nfe.getMessage());
			}
		}
		return results;
	}
}
