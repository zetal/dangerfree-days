package bitevo.zetal.dfdays;

import org.apache.logging.log4j.Logger;

import bitevo.zetal.dfdays.handler.DFDaysEventHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod(modid = DFDaysMod.MODID, name = DFDaysMod.NAME, version = DFDaysMod.VERSION)
public class DFDaysMod
{
    @SidedProxy(serverSide= "bitevo.zetal.dfdays.DFDaysCommonProxy", clientSide = "bitevo.zetal.dfdays.DFDaysClientProxy")
    public static DFDaysCommonProxy proxy;
    
    public static final String MODID = "dfdays";
    public static final String NAME = "Dangerfree Days";
    public static final String VERSION = "1.0";

    private static Logger logger;
    
	@Instance(MODID)
	public static DFDaysMod instance;

	@EventHandler
    public void init(FMLInitializationEvent event)
    {
		MinecraftForge.EVENT_BUS.register(new DFDaysEventHandler());
    }

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		proxy.registerRenderInformation();
	}
}
